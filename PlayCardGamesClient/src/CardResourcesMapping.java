import java.util.HashMap;

/**
 * Created by daniel on 28-02-2016.
 */
public class CardResourcesMapping {
    public static final HashMap<Constants.CardSuit, HashMap<String, String>> values = new HashMap<Constants.CardSuit, HashMap<String, String>>() {
        {
            put(Constants.CardSuit.COPAS, new HashMap<String, String>());
            put(Constants.CardSuit.ESPADAS, new HashMap<String, String>());
            put(Constants.CardSuit.OUROS, new HashMap<String, String>());
            put(Constants.CardSuit.PAUS, new HashMap<String, String>());

            get(Constants.CardSuit.COPAS).put("2", "2_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("3", "3_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("4", "4_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("5", "5_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("6", "6_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("7", "7_of_hearts.png");
            get(Constants.CardSuit.COPAS).put("Q", "queen_of_hearts2.png");
            get(Constants.CardSuit.COPAS).put("J", "jack_of_hearts2.png");
            get(Constants.CardSuit.COPAS).put("K", "king_of_hearts2.png");
            get(Constants.CardSuit.COPAS).put("A", "ace_of_hearts.png");

            get(Constants.CardSuit.ESPADAS).put("2", "2_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("3", "3_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("4", "4_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("5", "5_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("6", "6_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("7", "7_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("Q", "queen_of_spades2.png");
            get(Constants.CardSuit.ESPADAS).put("J", "jack_of_spades2.png");
            get(Constants.CardSuit.ESPADAS).put("K", "king_of_spades.png");
            get(Constants.CardSuit.ESPADAS).put("A", "ace_of_spades.png");

            get(Constants.CardSuit.OUROS).put("2", "2_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("3", "3_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("4", "4_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("5", "5_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("6", "6_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("7", "7_of_diamonds.png");
            get(Constants.CardSuit.OUROS).put("Q", "queen_of_diamonds2.png");
            get(Constants.CardSuit.OUROS).put("J", "jack_of_diamonds2.png");
            get(Constants.CardSuit.OUROS).put("K", "king_of_diamonds2.png");
            get(Constants.CardSuit.OUROS).put("A", "ace_of_diamonds.png");

            get(Constants.CardSuit.PAUS).put("2", "2_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("3", "3_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("4", "4_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("5", "5_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("6", "6_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("7", "7_of_clubs.png");
            get(Constants.CardSuit.PAUS).put("Q", "queen_of_clubs2.png");
            get(Constants.CardSuit.PAUS).put("J", "jack_of_clubs2.png");
            get(Constants.CardSuit.PAUS).put("K", "king_of_clubs2.png");
            get(Constants.CardSuit.PAUS).put("A", "ace_of_clubs.png");
        }
    };
}
