import javax.swing.*;
import java.awt.event.*;
import java.rmi.RemoteException;

/**
 * Created by daniel on 25-12-2015.
 */
public class LoginPage extends JFrame{
    private Client client;
    private JPanel panel1;
    private JTextField textField1;
    private JPasswordField passwordField1;
    private JButton loginButton;
    private JPanel JpanelGreeting;
    private JPanel JPanelInputs;
    private JPanel JPanelLogin;
    private JPanel JPanelGreetingImage;
    private JLabel GreetingImage;
    private JButton loginAsGuestButton;

    public LoginPage(Client client)
    {
        setClient(client);
        initUI();

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String username = textField1.getText();
                String password = passwordField1.getText();
                try {
                    int response = getClient().login(username, password);
                    if( response >= 0)
                    {
                        JOptionPane.showMessageDialog (null, "Welcome my friend " + username + " !", "Login", JOptionPane.INFORMATION_MESSAGE);
                        getClient().goToLobby();
                    }
                    else if(response == -1)
                    {
                        JOptionPane.showMessageDialog (null, "Login Failed, username or password incorrect", "Login", JOptionPane.INFORMATION_MESSAGE);
                    }
                    else if(response == -2)
                    {
                        JOptionPane.showMessageDialog (null, "Login Failed, user already online", "Login", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        loginAsGuestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    getClient().loginAsGuest();
                    JOptionPane.showMessageDialog (null, "Welcome, be my guest!", "Login", JOptionPane.INFORMATION_MESSAGE);
                    getClient().goToLobby();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void initUI()
    {

        setTitle("Play Card Games");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        String currentPath = this.getClass().getClassLoader().getResource("").getPath();
        String imagePath = currentPath + "Resources/JakeCardGame.png";
        ImageIcon imageIcon = new ImageIcon(imagePath, "");

        GreetingImage.setIcon(imageIcon);

        setContentPane(panel1);
        pack();
        setVisible(true);
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
