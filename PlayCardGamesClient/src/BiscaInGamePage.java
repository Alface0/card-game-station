import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.rmi.RemoteException;
import java.util.*;
import java.util.List;

/**
 * Created by daniel on 23-02-2016.
 */
public class BiscaInGamePage extends JFrame{
    Client client;
    volatile boolean viewRefreshing;
    int gameIdentifier;
    int gamePosition;
    ArrayList<CardView> hand;
    HashMap<String, JPanel> tableAreas;
    ArrayList<AbstractMap.SimpleEntry<Card, String>> localTableCards;
    private JPanel container;
    private JPanel playerOne;
    private JPanel playerTwo;
    private JPanel playerThree;
    private JPanel tablePlayerOne;
    private JPanel tablePlayerTwo;
    private JPanel tablePlayerThree;
    private JPanel timeCounter;
    private JPanel playerOneHand;
    private JLabel playerOneName;
    private JPanel playerThreeHand;
    private JLabel playerThreeName;
    private JLabel playerTwoName;
    private JPanel playerTwoHand;
    private JLabel time;
    private JLabel currentPlayingPlayer;
    private JLabel deckCardsNumber;
    private JLabel gameSuit;

    public BiscaInGamePage(Client client, int gameIdentifier) {
        this.client = client;
        this.gameIdentifier = gameIdentifier;
        viewRefreshing = true;
        hand = new ArrayList<CardView>();
        tableAreas = new HashMap<String, JPanel>();
        localTableCards = new ArrayList<AbstractMap.SimpleEntry<Card, String>>();

        setPlayersNames();
        setGameSuit();
        WindowUtils.initWindow(this, 800, 600, client, container);
        playerOneHand.setLayout(new BoxLayout(playerOneHand, BoxLayout.PAGE_AXIS));
        playerThreeHand.setLayout(new BoxLayout(playerThreeHand, BoxLayout.PAGE_AXIS));

        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "Are You Sure to leave the gaming station?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        System.out.println("Trying to logout");
                        getClient().logout();
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        };
        addWindowListener(exitListener);

        new Thread(new Runnable() {
            @Override
            public void run() {
                long tempoAnterior = System.currentTimeMillis();
                while (viewRefreshing)
                {
                    if (System.currentTimeMillis() > tempoAnterior) {
                        try {
                            if(!getClient().isGameFinished())
                            {
                                RefreshPage();
                            }
                            else
                            {
                                getClient().goToBiscaEndGameRoom();
                            }
                        } catch (Exception exception)
                        {
                            //Little possibility of failing, not worth of doing really hard stuff
                        }
                        tempoAnterior += 50;
                    }
                }
            }
        }).start();
    }

    private void setGameSuit() {
        String currentPath = this.getClass().getClassLoader().getResource("").getPath();
        Constants.CardSuit cardSuit = getClient().getGameSuit();
        String imagePath = "";
        switch (cardSuit)
        {
            case COPAS:
                imagePath = currentPath + "Resources/hearts.png";
                break;
            case PAUS:
                imagePath = currentPath + "Resources/clubs.png";
                break;
            case ESPADAS:
                imagePath = currentPath + "Resources/spades.png";
                break;
            case OUROS:
                imagePath = currentPath + "Resources/diamonds.png";
                break;
        }
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(32, 32, Image.SCALE_DEFAULT));
        gameSuit.setIcon(imageIcon);

    }

    private void setPlayersNames() {
        ArrayList<String> playersName = getClient().getGamePlayersName();
        gamePosition = getInGamePosition(playersName);
        int currentPosition = new Integer(gamePosition);

        playerTwoName.setText(playersName.get(currentPosition));
        tableAreas.put(playersName.get(currentPosition), tablePlayerTwo);
        currentPosition = getNextGamePosition(playersName, currentPosition);

        playerThreeName.setText(playersName.get(currentPosition));
        tableAreas.put(playersName.get(currentPosition), tablePlayerThree);
        currentPosition = getNextGamePosition(playersName, currentPosition);


        if(playersName.size() == 3) {
            playerOneName.setText(playersName.get(currentPosition));
            tableAreas.put(playersName.get(currentPosition), tablePlayerOne);
        }
        else
        {
            playerOneName.setText("");
        }
    }

    private int getNextGamePosition(List playersName, int currentPosition) {
        currentPosition++;
        if(playersName.size() == currentPosition)
        {
            return 0;
        }

        return currentPosition;
    }

    private int getInGamePosition(ArrayList<String> playersName) {
        for(int i = 0; i < playersName.size() ; i++)
            if(getClient().getUsername().equals(playersName.get(i)))
                return i;

        return -1;
    }

    public Client getClient() {
        return client;
    }

    public void RefreshPage() {
        time.setText("" + getClient().getGameCurrentTurnTime());
        currentPlayingPlayer.setText(getClient().getCurrentPlayerName());
        deckCardsNumber.setText("" + getClient().getDeckCardsNumber());
        refreshHand();
        refreshOtherPlayersHands();
        refreshTable();
    }


    private void refreshTable() {
        ArrayList<AbstractMap.SimpleEntry<Card, String>> tableCards = getClient().getTableCards();
        if(localTableCards.size() != tableCards.size())
        {
            localTableCards = tableCards;

            clearTable();

            for(AbstractMap.SimpleEntry<Card, String> tablePlay: tableCards)
                addCardToTablePosition(tableAreas.get(tablePlay.getValue()), tablePlay.getKey());
        }
    }

    private void clearTable()
    {
        for(Map.Entry<String, JPanel> tableArea: tableAreas.entrySet()) {
            JPanel jPanel = tableArea.getValue();
            jPanel.removeAll();
            jPanel.revalidate();
            jPanel.repaint();
        }
    }

    private void addCardToTablePosition(JPanel jPanel, Card card)
    {
        try {
            jPanel.add(new CardView(card));
        } catch (Exception exception) {
            // Ignore
        }

        jPanel.revalidate();
        jPanel.repaint();
    }

    private void refreshOtherPlayersHands() {
        ArrayList<Integer> playersCardCount = getClient().getPlayersCardCount();
        int currentPosition = new Integer(gamePosition);

        currentPosition = getNextGamePosition(playersCardCount ,currentPosition);
        repaintOtherHand(playerThreeHand, playersCardCount.get(currentPosition));

        if(playersCardCount.size() > 2)
        {
            currentPosition = getNextGamePosition(playersCardCount ,currentPosition);
            repaintOtherHand(playerOneHand, playersCardCount.get(currentPosition));
        }
    }

    public void repaintOtherHand(JPanel playerHand, int cardCount)
    {
        if(cardCount != playerHand.getComponentCount())
        {
            playerHand.removeAll();
            for (int i = 0; i < cardCount; i++) {
                playerHand.add(new CardView(Constants.Orientation.HORIZONTAL));
            }
            playerHand.revalidate();
            playerHand.repaint();
        }
    }

    public void refreshHand()
    {
        ArrayList<Card> cards = getClient().getMyHand(gamePosition);
        if(cards.size() != playerTwoHand.getComponentCount())
        {
            updateHand(cards);
            playerTwoHand.removeAll();
            for (CardView cardView : hand) {
                playerTwoHand.add(cardView);
            }
            playerTwoHand.revalidate();
            playerTwoHand.repaint();
        }
    }

    private void updateHand(ArrayList<Card> cards)
    {
        hand = new ArrayList<CardView>();
        for(Card card: cards)
        {
            hand.add(new CardView(card, getClient()));
        }
    }

}
