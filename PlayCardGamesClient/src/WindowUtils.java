import javax.swing.*;
import java.awt.*;

/**
 * Created by daniel on 25-02-2016.
 */
public class WindowUtils {
    static public void initWindow(JFrame jFrame, int sizeX, int sizeY, Client client, Container container)
    {
        jFrame.setTitle("Play Card Games - " + client.getUsername());
        jFrame.setSize(sizeX, sizeY);
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        jFrame.setContentPane(container);
        jFrame.setVisible(true);
    }

}
