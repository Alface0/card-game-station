import javax.swing.*;
import java.awt.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 24-12-2015.
 */
public class Client{
    ControllerInterface controller;
    InQueueControllerInterface inQueueController;
    InGameControllerInterface inGameController;
    private int identifier;
    private String username;
    ArrayList<AbstractMap.SimpleEntry<Integer, String>> lastPlayedGames;
    JFrame window;

    public Client()
    {
        try {
            Registry registry = LocateRegistry.getRegistry("localhost", Constants.CONTROLLER_RMI_PORT);
            controller = (ControllerInterface) registry.lookup(Constants.CONTROLLER_RMI_ID);

            Registry inQueueRegistry = LocateRegistry.getRegistry("localhost", Constants.IN_QUEUE_CONTROLLER_RMI_PORT);
            inQueueController = (InQueueControllerInterface) inQueueRegistry.lookup(Constants.IN_QUEUE_CONTROLLER_RMI_ID);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                window = new LoginPage(Client.this);
            }
        });
    }

    public void setIdentifier(int identifier)
    {
        this.identifier = identifier;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIdentifier() {
        return identifier;
    }

    public int login(String username, String password) throws RemoteException {
        int response = controller.login(username, password);
        if(response >= 0) {
            setIdentifier(response);
            setUsername(username);
        }

        return response;
    }

    public int loginAsGuest() throws RemoteException {
        int response = controller.loginAsGuest();
        if(response >= 0)
        {
            setIdentifier(response);
            setUsername("Guest-" + response);
        }
        return response;
    }

    public void logout() throws RemoteException {
        controller.logout(getIdentifier());
    }

    public void goToLobby() {
        window.dispose();
        window = new LobbyPage(this);
    }

    public Vector getInstalledGamesList()
    {
        Vector gamesList = new Vector();
        for(String game : Constants.GAMELIST)
            gamesList.add(game);

        return gamesList;
    }

    public Vector getOpenGamesList()
    {
        try {
            return controller.getOpenGamesList();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new Vector();
    }

    public Vector getUserLastGames()
    {
        Vector vector = new Vector();
        try {
            lastPlayedGames = new ArrayList<>(controller.getUserLastGames(getIdentifier()));
            for(AbstractMap.SimpleEntry<Integer, String> game: lastPlayedGames)
            {
                vector.add(game.getValue());
            }
            return vector;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return vector;
    }

    public void createGameRoom(String gameName) {
        try {
            int gameBuilderIdentifier = controller.createNewMatch(getIdentifier(), gameName);
            goToBiscaQueue(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void goToBiscaQueue(int gameBuilderIdentifier) {
        window.dispose();
        window = new BiscaQueuePage(this, gameBuilderIdentifier);
    }

    public int getPlayersNumber(int gameBuilderIdentifier) {
        try {
            return inQueueController.getPlayersNumber(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getMaxPlayersNumber(int gameBuilderIdentifier) {
        try {
            return inQueueController.getMaxPlayersNumber(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean canStartGame(int gameBuilderIdentifier){
        try {
            return inQueueController.canStartGame(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Vector getPlayersList(int gameBuilderIdentifier) {
        try {
            return inQueueController.getPlayersList(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void joinOpenGame(String openGameName) {
        try {
            int builderIdentifier = controller.joinOpenGame(getIdentifier(), openGameName);
            if(builderIdentifier >= 0)
                goToBiscaQueue(builderIdentifier);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public String getGameName(int gameBuilderIdentifier) {
        try {
            return inQueueController.getRoomName(gameBuilderIdentifier);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return "";
    }

    public void returnToLobby(int gameBuilderIdentifier) {
        try {
            inQueueController.returnToLobby(gameBuilderIdentifier, getIdentifier());
            goToLobby();

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addBot(int gameBuilderIdentifier, String level) {
        try {
            inQueueController.addBot(gameBuilderIdentifier, level);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void startGame(int gameBuilderIdentifier) {
        try {
            int gameIdentifier = inQueueController.startGame(gameBuilderIdentifier, getIdentifier());
            if(gameIdentifier >= 0)
            {
                String gameName = inQueueController.getGameName(gameBuilderIdentifier);
                enterInGame(gameIdentifier, gameName);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void enterInGame(int gameIdentifier, String gameName) {
        BiscaQueuePage biscaQueuePage = (BiscaQueuePage) window;
        biscaQueuePage.viewRefreshing = false;
        registerInGameController(gameIdentifier);

        try {
            if(gameName.equals("Bisca"))
                enterInGameBisca(gameIdentifier);
            else if(gameName.equals("Sueca"))
                enterInGameSueca(gameIdentifier);
        } catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void enterInGameSueca(int gameIdentifier) {
    }

    private void enterInGameBisca(int gameIdentifier) {
        window.dispose();
        window = new BiscaInGamePage(this, gameIdentifier);
    }

    public void isGameStarted(int builderIdentifier)
    {
        try {
            int gameIdentifier = inQueueController.isGameStarted(builderIdentifier, getIdentifier());
            if(gameIdentifier >= 0)
            {
                enterInGame(gameIdentifier ,inQueueController.getGameName(builderIdentifier));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void registerInGameController(int gameIdentifier)
    {
        Registry registry;
        try {
            registry = LocateRegistry.getRegistry("localhost", controller.getGamePort(gameIdentifier));
            inGameController = (InGameControllerInterface) registry.lookup(Constants.IN_GAME_CONTROLLER_RMI_ID);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPlayerNumber()
    {
        try {
            return inGameController.getCurrentPlayerNumber();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int getGameCurrentTurnTime() {
        try {
            return inGameController.getCurrentTurnTime();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public ArrayList<String> getGamePlayersName() {
        try {
            return inGameController.getGamePlayersName();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCurrentPlayerName() {
        try {
            return inGameController.getCurrentPlayerName();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";
    }

    public int getDeckCardsNumber() {
        try {
            return inGameController.getDeckCardsNumber();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public ArrayList<Card> getMyHand(int gamePosition) {
        try {
            return inGameController.getMyHand(gamePosition);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Integer> getPlayersCardCount() {
        try {
            return inGameController.getPlayersCardCount();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Constants.CardSuit getGameSuit() {
        try {
            return inGameController.getGameTrumpSuit();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void playCard(Card card) {
        try {
            inGameController.playCard(getIdentifier(), card);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<AbstractMap.SimpleEntry<Card, String>> getTableCards() {
        try {
            return inGameController.getTableCards();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isGameFinished() {
        try {
            return inGameController.isGameFinished();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void goToBiscaEndGameRoom() {
        BiscaInGamePage biscaInGamePage = (BiscaInGamePage) window;
        biscaInGamePage.viewRefreshing = false;

        window.dispose();
        window = new BiscaEndGamePage(this);
    }

    public Vector getPlayerScoreList() {
        try {
            return inGameController.getPlayersScoreList();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getCurrentGameTurn() {
        try {
            return inGameController.getCurrentGameTurn();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String getGameTime() {
        try {
            return inGameController.getGameTime();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getGameWinner() {
        try {
            return inGameController.getGameWinner();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void returnFromGameToLobby() {
        try {
            inGameController.returnFromGameToLobby(getIdentifier());
            inGameController = null;
            goToLobby();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void goToBiscaReplayPage(String game)
    {
        int gameDatabaseID = -1;
        for(AbstractMap.SimpleEntry<Integer, String> pair: lastPlayedGames)
        {
            if(game.equals(pair.getValue())) {
                gameDatabaseID = pair.getKey();
            }
        }
        if(gameDatabaseID != -1) {

            int gameIdentifier;
            try {
                gameIdentifier = controller.startGameReplay(gameDatabaseID);
                registerInGameController(gameIdentifier);
                goToReplayRoom(gameIdentifier);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }

    }

    private void goToReplayRoom(int gameIdentifier) {
        window.dispose();
        window = new BiscaGameReplayPage(this, gameIdentifier);
    }

    public void moveGameBack()
    {
        try {
            inGameController.moveGameBack();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void moveGameForward()
    {
        try {
            inGameController.moveGameForward();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public String getGameCurrentReplayMove() {
        String moves = "";
        try {
            moves = inGameController.getCurrentReplayMove();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return moves;
    }

    public void returnFromReplayToLobby() {
        try {
            inGameController.returnFromReplayToLobby();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        goToLobby();
    }
}
