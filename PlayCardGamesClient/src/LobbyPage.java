import javax.swing.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.Vector;

/**
 * Created by daniel on 26-12-2015.
 */
public class LobbyPage extends JFrame {
    volatile boolean viewRefreshing;
    private Client client;
    private JPanel Container;
    private JList list1;
    private JButton createNewGameButton;
    private JList lastGames;
    private JButton replayTheGameButton;
    private JList openGamesList;
    private JButton joinGameButton;
    private JButton quitButton;
    private JButton statisticsButton;
    private JButton infoButton;
    private JLabel GreetingsMessage;
    private JLabel CardGameStationLogo;

    public LobbyPage(Client client){
        viewRefreshing = true;
        setClient(client);
        initUI();

        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "Are You Sure to leave the gaming station?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        System.out.println("Trying to logout");
                        viewRefreshing = false;
                        getClient().logout();
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        };
        addWindowListener(exitListener);



        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    viewRefreshing = false;
                    getClient().logout();
                    System.exit(0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        createNewGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try
                {
                    String game = (String) list1.getSelectedValue();
                    if(game != null) {
                        getClient().createGameRoom(game);
                    }
                }
                catch (Exception exception)
                {
                    //Do nothing
                }
            }
        });

        joinGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String openGameName = (String) openGamesList.getSelectedValue();
                getClient().joinOpenGame(openGameName);
            }
        });

        replayTheGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String gameName = (String) lastGames.getSelectedValue();
                getClient().goToBiscaReplayPage(gameName);
            }
        });
    }

    private void initUI() {
        setTitle("Play Card Games - " + getClient().getUsername());
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Vector installedGamesList = getClient().getInstalledGamesList();
        list1.setListData(installedGamesList);


        new Thread(new Runnable() {
            @Override
            public void run() {
                long nextTime = System.currentTimeMillis();
                while(viewRefreshing)
                {
                    long currentTime = System.currentTimeMillis();
                    if(currentTime >= nextTime)
                    {
                        Vector games = getClient().getOpenGamesList();
                        openGamesList.setListData(games);

                        nextTime += 5000;
                    }
                }
            }
        }).start();

        Vector userLastGames = getClient().getUserLastGames();
        lastGames.setListData(userLastGames);

        //getGreetingsMessage().setText("Welcome " + getClient().getUsername());

        String currentPath = this.getClass().getClassLoader().getResource("").getPath();
        String imagePath = currentPath + "Resources/LogoCardGamesStation.png";
        ImageIcon imageIcon = new ImageIcon(imagePath, "");

        CardGameStationLogo.setIcon(imageIcon);

        setContentPane(Container);
        pack();
        setVisible(true);
    }

    public JLabel getGreetingsMessage()
    {
        return GreetingsMessage;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
