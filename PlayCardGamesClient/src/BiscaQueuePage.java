import javax.swing.*;
import java.awt.event.*;
import java.rmi.RemoteException;

/**
 * Created by daniel on 11-02-2016.
 */
public class BiscaQueuePage extends JFrame {
    Client client;
    int gameBuilderIdentifier;
    volatile boolean viewRefreshing;
    private JRadioButton easyRadioButton;
    private JButton addButton;
    private JList playersList;
    private JButton returnToLobbyButton;
    private JButton startGameButton;
    private JPanel container;
    private JLabel slotsNumber;
    private JLabel stateLabel;
    private JLabel gameName;

    public BiscaQueuePage(Client client, final int gameBuilderIdentifier)
    {
        viewRefreshing = true;
        this.gameBuilderIdentifier = gameBuilderIdentifier;
        setClient(client);
        WindowUtils.initWindow(this, 600, 400, client, container);

        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "Are You Sure to leave the gaming station?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        System.out.println("Trying to logout");
                        viewRefreshing = false;
                        getClient().returnToLobby(gameBuilderIdentifier);
                        getClient().logout();
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        };
        addWindowListener(exitListener);

        returnToLobbyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                viewRefreshing = false;
                getClient().returnToLobby(gameBuilderIdentifier);
            }
        });

        refreshPage();

        //Refresh the view every 10 seconds
        new Thread(new Runnable() {
            @Override
            public void run() {

                long tempoAnterior = System.currentTimeMillis();
                while (viewRefreshing)
                {
                    if (System.currentTimeMillis() > tempoAnterior) {
                        try {
                            refreshPage();
                        } catch (Exception exception)
                        {
                            //Little possibility of failing, not worth of doing really hard stuff
                        }
                        tempoAnterior += 500;
                    }
                }
            }
        }).start();

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(easyRadioButton.isSelected())
                    getClient().addBot(gameBuilderIdentifier, "Easy");
            }
        });

        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getClient().startGame(gameBuilderIdentifier);
            }
        });

    }

    private void refreshPage() {
        gameName.setText(getClient().getGameName(gameBuilderIdentifier));
        updateSlotsLabel(getClient().getPlayersNumber(gameBuilderIdentifier), getClient().getMaxPlayersNumber(gameBuilderIdentifier));
        updateStateLabel(getClient().canStartGame(gameBuilderIdentifier));
        playersList.setListData(getClient().getPlayersList(gameBuilderIdentifier));
        getClient().isGameStarted(gameBuilderIdentifier);
    }

    public void updateSlotsLabel(int slotsFilled, int totalSlots)
    {
        String text = "Slots: " + slotsFilled + " / " + totalSlots;
        slotsNumber.setText(text);
    }

    public void updateStateLabel(boolean canStart)
    {
        if(canStart)
            stateLabel.setText("State: Ready");
        else
            stateLabel.setText("State: Not Ready");
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
