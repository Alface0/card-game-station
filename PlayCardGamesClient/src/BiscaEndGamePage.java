import javax.swing.*;
import java.awt.event.*;
import java.rmi.RemoteException;

/**
 * Created by daniel on 06-03-2016.
 */
public class BiscaEndGamePage extends JFrame{
    Client client;
    private JPanel container;
    private JList playersList;
    private JButton returnToLobbyButton;
    private JLabel turnsNumber;
    private JLabel gameTime;
    private JLabel gameWinner;

    public BiscaEndGamePage(final Client client)
    {
        this.client = client;
        WindowUtils.initWindow(this, 400, 600, client, container);
        playersList.setListData(client.getPlayerScoreList());
        turnsNumber.setText("Turns Number: " + client.getCurrentGameTurn());
        gameTime.setText("Game Time: " + client.getGameTime());
        gameWinner.setText("Winner: " + client.getGameWinner());

        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "Are You Sure to leave the gaming station?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        System.out.println("Trying to logout");
                        client.logout();
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        };
        addWindowListener(exitListener);

        returnToLobbyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                client.returnFromGameToLobby();
            }
        });
    }

}
