import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.*;

/**
 * Created by daniel on 07-03-2016.
 */
public class BiscaGameReplayPage extends JFrame {
    Client client;
    int gameIdentifier;
    int gamePosition;
    ArrayList<CardView> hand;
    HashMap<String, JPanel> tableAreas;
    ArrayList<AbstractMap.SimpleEntry<Card, String>> localTableCards;
    private JPanel playerTwo;
    private JLabel playerTwoName;
    private JPanel playerTwoHand;
    private JPanel playerThree;
    private JLabel playerThreeName;
    private JPanel playerThreeHand;
    private JPanel playerOne;
    private JLabel playerOneName;
    private JPanel playerOneHand;
    private JPanel timeCounter;
    private JLabel move;
    private JPanel tablePlayerOne;
    private JPanel tablePlayerThree;
    private JPanel tablePlayerTwo;
    private JLabel deckCardsNumber;
    private JLabel currentPlayingPlayer;
    private JLabel gameSuit;
    private JPanel container;
    private JButton last;
    private JButton next;
    private JButton returnToLobbyButton;

    public BiscaGameReplayPage(Client client, int gameIdentifier) {
        this.client = client;
        this.gameIdentifier = gameIdentifier;
        hand = new ArrayList<>();
        tableAreas = new HashMap<>();
        localTableCards = new ArrayList<>();

        setPlayersNames();
        setGameSuit();

        WindowUtils.initWindow(this, 800, 600, client, container);

        playerOneHand.setLayout(new BoxLayout(playerOneHand, BoxLayout.PAGE_AXIS));
        playerThreeHand.setLayout(new BoxLayout(playerThreeHand, BoxLayout.PAGE_AXIS));

        refreshPage();

        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "Are You Sure to leave the gaming station?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        System.out.println("Trying to logout");
                        getClient().logout();
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        };
        addWindowListener(exitListener);

        last.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getClient().moveGameBack();
                refreshPage();
            }
        });

        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getClient().moveGameForward();
                refreshPage();
            }
        });

        returnToLobbyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getClient().returnFromReplayToLobby();
            }
        });
    }

    private void setGameSuit() {
        String currentPath = this.getClass().getClassLoader().getResource("").getPath();
        Constants.CardSuit cardSuit = getClient().getGameSuit();
        String imagePath = "";
        switch (cardSuit)
        {
            case COPAS:
                imagePath = currentPath + "Resources/hearts.png";
                break;
            case PAUS:
                imagePath = currentPath + "Resources/clubs.png";
                break;
            case ESPADAS:
                imagePath = currentPath + "Resources/spades.png";
                break;
            case OUROS:
                imagePath = currentPath + "Resources/diamonds.png";
                break;
        }
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(32, 32, Image.SCALE_DEFAULT));
        gameSuit.setIcon(imageIcon);

    }

    private void setPlayersNames() {
        ArrayList<String> playersName = getClient().getGamePlayersName();
        gamePosition = getInGamePosition(playersName);
        int currentPosition = new Integer(gamePosition);

        playerTwoName.setText(playersName.get(currentPosition));
        tableAreas.put(playersName.get(currentPosition), tablePlayerTwo);
        currentPosition = getNextGamePosition(playersName, currentPosition);

        playerThreeName.setText(playersName.get(currentPosition));
        tableAreas.put(playersName.get(currentPosition), tablePlayerThree);
        currentPosition = getNextGamePosition(playersName, currentPosition);


        if(playersName.size() == 3) {
            playerOneName.setText(playersName.get(currentPosition));
            tableAreas.put(playersName.get(currentPosition), tablePlayerOne);
        }
        else
        {
            playerOneName.setText("");
        }
    }

    private int getNextGamePosition(java.util.List playersName, int currentPosition) {
        currentPosition++;
        if(playersName.size() == currentPosition)
        {
            return 0;
        }

        return currentPosition;
    }

    private int getInGamePosition(ArrayList<String> playersName) {
        for(int i = 0; i < playersName.size() ; i++)
            if(getClient().getUsername().equals(playersName.get(i)))
                return i;

        return -1;
    }

    public Client getClient() {
        return client;
    }

    public void refreshPage() {
        move.setText("" + getClient().getGameCurrentReplayMove());
        currentPlayingPlayer.setText(getClient().getCurrentPlayerName());
        deckCardsNumber.setText("" + getClient().getDeckCardsNumber());
        refreshHand();
        refreshOtherPlayersHands();
        refreshTable();
    }


    private void refreshTable() {
        ArrayList<AbstractMap.SimpleEntry<Card, String>> tableCards = getClient().getTableCards();
        if(localTableCards.size() != tableCards.size())
        {
            localTableCards = tableCards;

            clearTable();

            for(AbstractMap.SimpleEntry<Card, String> tablePlay: tableCards)
                addCardToTablePosition(tableAreas.get(tablePlay.getValue()), tablePlay.getKey());
        }
    }

    private void clearTable()
    {
        for(Map.Entry<String, JPanel> tableArea: tableAreas.entrySet()) {
            JPanel jPanel = tableArea.getValue();
            jPanel.removeAll();
            jPanel.revalidate();
            jPanel.repaint();
        }
    }

    private void addCardToTablePosition(JPanel jPanel, Card card)
    {
        try {
            jPanel.add(new CardView(card));
        } catch (Exception exception) {
            // Ignore
        }

        jPanel.revalidate();
        jPanel.repaint();
    }

    private void refreshOtherPlayersHands() {
        ArrayList<Integer> playersCardCount = getClient().getPlayersCardCount();
        int currentPosition = new Integer(gamePosition);

        currentPosition = getNextGamePosition(playersCardCount ,currentPosition);
        repaintOtherHand(playerThreeHand, playersCardCount.get(currentPosition));

        if(playersCardCount.size() > 2)
        {
            currentPosition = getNextGamePosition(playersCardCount ,currentPosition);
            repaintOtherHand(playerOneHand, playersCardCount.get(currentPosition));
        }
    }

    public void repaintOtherHand(JPanel playerHand, int cardCount)
    {
        if(cardCount != playerHand.getComponentCount())
        {
            playerHand.removeAll();
            for (int i = 0; i < cardCount; i++) {
                playerHand.add(new CardView(Constants.Orientation.HORIZONTAL));
            }
            playerHand.revalidate();
            playerHand.repaint();
        }
    }

    public void refreshHand()
    {
        ArrayList<Card> cards = getClient().getMyHand(gamePosition);
        if(cards.size() != playerTwoHand.getComponentCount())
        {
            updateHand(cards);
            playerTwoHand.removeAll();
            for (CardView cardView : hand) {
                playerTwoHand.add(cardView);
            }
            playerTwoHand.revalidate();
            playerTwoHand.repaint();
        }
    }

    private void updateHand(ArrayList<Card> cards)
    {
        hand = new ArrayList<CardView>();
        for(Card card: cards)
        {
            hand.add(new CardView(card, 64, 96));
        }
    }
}
