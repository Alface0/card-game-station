import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by daniel on 28-02-2016.
 */
public class CardView extends JLabel{
    String currentPath = this.getClass().getClassLoader().getResource("").getPath();
    Card card;

    public CardView(Constants.Orientation orientation)
    {
        ImageIcon imageIcon;
        if(orientation == Constants.Orientation.VERTICAL) {
            String imagePath = currentPath + "Resources/card_back_vertical.png";
            imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(64, 96, Image.SCALE_DEFAULT));
        }
        else
        {
            String imagePath = currentPath + "Resources/card_back_horizontal.png";
            imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(96, 64, Image.SCALE_DEFAULT));
        }
        setIcon(imageIcon);
        setAlignmentX(0.5f);
    }

    public CardView(Card card)
    {
        this.card = card;
        String imagePath = getCardResourcePath();
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(48, 64, Image.SCALE_DEFAULT));
        setIcon(imageIcon);
    }

    public CardView(Card card, int sizeX, int sizeY)
    {
        this.card = card;
        String imagePath = getCardResourcePath();
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(sizeX, sizeY, Image.SCALE_DEFAULT));
        setIcon(imageIcon);
    }

    public CardView(final Card card, final Client client)
    {
        this.card = card;
        String imagePath = getCardResourcePath();
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(imagePath).getImage().getScaledInstance(64, 96, Image.SCALE_DEFAULT));
        setIcon(imageIcon);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                client.playCard(card);
            }
        });
    }


    private String getCardResourcePath() {
        return currentPath + "Resources/" + CardResourcesMapping.values.get(card.suit).get(card.value);
    }

    public String toString()
    {
        return card.toString() + " image-> " + getCardResourcePath();
    }
}
