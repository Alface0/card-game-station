import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 10-02-2016.
 */
public interface InGameControllerInterface extends Remote {
    void playCard(int userIdentifier, Card card) throws RemoteException;
    int getCurrentPlayerNumber() throws RemoteException;
    void returnToLobby(int userIdentifier) throws RemoteException;
    int getCurrentTurnTime() throws RemoteException;
    ArrayList<String> getGamePlayersName() throws RemoteException;
    String getCurrentPlayerName() throws RemoteException;
    int getDeckCardsNumber() throws RemoteException;
    ArrayList<Card> getMyHand(int gamePosition) throws RemoteException;
    ArrayList<Integer> getPlayersCardCount() throws RemoteException;
    Constants.CardSuit getGameTrumpSuit() throws RemoteException;
    ArrayList<AbstractMap.SimpleEntry<Card, String>> getTableCards() throws RemoteException;
    boolean isGameFinished() throws RemoteException;
    Vector getPlayersScoreList() throws RemoteException;
    int getCurrentGameTurn() throws RemoteException;
    String getGameTime() throws RemoteException;
    String getGameWinner() throws RemoteException;
    void returnFromGameToLobby(int userIdentifier) throws RemoteException;
    void moveGameBack() throws RemoteException;
    void moveGameForward() throws RemoteException;
    String getCurrentReplayMove() throws RemoteException;
    void returnFromReplayToLobby() throws RemoteException;
}
