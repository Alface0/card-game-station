import java.util.Collections;
import java.util.HashMap;

/**
 * Created by daniel on 25-12-2015.
 */
public class Constants {
    public static final String CONTROLLER_RMI_ID = "ControllerInterface";
    public static final int CONTROLLER_RMI_PORT = 2223;
    public static final String IN_QUEUE_CONTROLLER_RMI_ID = "InQueueControllerInterface";
    public static final int IN_QUEUE_CONTROLLER_RMI_PORT = 2224;
    public static final String IN_GAME_CONTROLLER_RMI_ID = "InGameControllerInterface";
    public static final int IN_GAME_CONTROLLER_BASE_RMI_PORT = 2225;
    public static final String [] GAMELIST = new String[]{"Bisca", "Sueca"};
    public static final String [] USERTYPES = new String[]{"RegisteredUser", "Guest", "GameBot"};

    public enum Orientation {VERTICAL, HORIZONTAL}
    public enum CardSuit {ESPADAS, OUROS, COPAS, PAUS}
    public static final HashMap<String, Integer> values = new HashMap<String, Integer>()
    {
        {
        put("2", 1);
        put("3", 2);
        put("4", 3);
        put("5", 4);
        put("6", 5);
        put("Q", 6);
        put("J", 7);
        put("K", 8);
        put("7", 9);
        put("A", 10);
        }
    };

    public static final HashMap<String, Integer> cardsScores = new HashMap<String, Integer>()
    {
        {
            put("2", 0);
            put("3", 0);
            put("4", 0);
            put("5", 0);
            put("6", 0);
            put("Q", 2);
            put("J", 3);
            put("K", 4);
            put("7", 10);
            put("A", 11);
        }
    };
}
