import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

/**
 * Created by daniel on 21-02-2016.
 */
public interface InQueueControllerInterface extends Remote {
    Vector getPlayersList(int builderIdentifier) throws RemoteException;
    boolean addBot(int builderIdentifier, String level) throws RemoteException;
    void returnToLobby(int builderIdentifier, int userIdentifier) throws RemoteException;
    String getRoomName(int builderIdentifier) throws RemoteException;
    String getGameName(int builderIdentifier) throws RemoteException;
    int getPlayersNumber(int builderIdentifier) throws RemoteException;
    int getMaxPlayersNumber(int builderIdentifier) throws RemoteException;
    boolean canStartGame(int builderIdentifier) throws RemoteException;
    int startGame(int builderIdentifier, int userIdentifier) throws RemoteException;
    int isGameStarted(int builderIdentifier, int userIdentifier) throws RemoteException;
}
