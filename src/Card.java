import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by daniel on 28-12-2015.
 */
public class Card implements Comparable<Card>, Serializable{
    Constants.CardSuit suit;
    String value;

    public Card(Constants.CardSuit suit, String value)
    {
        this.suit=suit;
        this.value=value;
    }

    public int getNumericValue()
    {
        return Constants.values.get(value);
    }

    public int getCardScore() { return Constants.cardsScores.get(value);}

    @Override
    public int compareTo(Card card) {
        return this.getNumericValue() - card.getNumericValue();
    }

    @Override
    public boolean equals(Object object)
    {
        if(object instanceof Card) {
            Card card = (Card) object;
            if (this.suit == card.suit && this.value.equals(card.value)) {
                return true;
            }
        }

        return false;
    }

    public String toString()
    {
        return "Card from " + suit + " suit and with " + value + " value";
    }
}
