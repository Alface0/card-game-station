import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 24-12-2015.
 */
public interface ControllerInterface extends Remote {
    int login(String username, String password) throws RemoteException;
    int loginAsGuest() throws RemoteException;
    void logout(int userIdentifier) throws RemoteException;
    int createNewMatch(int userIdentifier, String gameName) throws RemoteException;
    Vector getOpenGamesList() throws RemoteException;
    ArrayList<AbstractMap.SimpleEntry<Integer, String>> getUserLastGames(int userIdentifier) throws RemoteException;
    int joinOpenGame(int userIdentifier, String openGameText) throws RemoteException;
    int getGamePort(int gameIdentifier) throws RemoteException;
    int startGameReplay(int gameDatabaseID) throws RemoteException;
}
