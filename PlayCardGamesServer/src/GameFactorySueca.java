/**
 * Created by daniel on 25-12-2015.
 */
public class GameFactorySueca extends GameFactory {
    @Override
    public GameRules getGameRules(Game game) {
        return null;
    }

    @Override
    public GameIA getGameIA() {
        return null;
    }

    @Override
    public Deck getGameDeck(int playersNumber) {
        return new SuecaDeck(playersNumber);
    }

    @Override
    public GameStartRules getGameStartRules() {
        return null;
    }
}
