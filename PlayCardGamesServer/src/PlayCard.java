import java.util.AbstractMap;

/**
 * Created by daniel on 10-02-2016.
 */
public class PlayCard implements GameMove {
    Game game;
    User user;
    Card card;

    public PlayCard(Game game, User user, Card card)
    {
        this.game = game;
        this.user = user;
        this.card = card;
    }

    @Override
    public void execute() {
        game.table.add(new AbstractMap.SimpleEntry<User, Card>(user ,card));
        game.playersHands.get(user).remove(card);
    }

    @Override
    public void undo() {
        game.table.remove(new AbstractMap.SimpleEntry<User, Card>(user ,card));
        game.playersHands.get(user).add(card);
    }

    @Override
    public Card getCard() {
        return this.card;
    }

    @Override
    public User getUser() {
        return this.user;
    }

    public String toString()
    {
        return "Move: " + game.toString() + " " + user.toString() + " " + card.toString();
    }

}
