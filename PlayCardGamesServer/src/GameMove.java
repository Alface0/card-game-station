/**
 * Created by daniel on 10-02-2016.
 */
public interface GameMove {
    public void execute();
    public void undo();
    public Card getCard();
    public User getUser();
}
