import java.util.ArrayList;

/**
 * Created by daniel on 25-12-2015.
 */
public abstract class GameFactory {

    public abstract GameRules getGameRules(Game game);
    public abstract GameIA getGameIA();
    public abstract Deck getGameDeck(int playersNumber);
    public abstract GameStartRules getGameStartRules();

    public static GameFactory getGame(String game)
    {
        if(game.equals("Sueca"))
            return new GameFactorySueca();
        else if(game.equals("Bisca"))
            return new GameFactoryBisca();
        else
            return null;
    }

    public static ArrayList<String> getGamesList()
    {
        ArrayList<String> gamesList = new ArrayList<String>();
        for(String game : Constants.GAMELIST)
            gamesList.add(game);

        return gamesList;
    }

}
