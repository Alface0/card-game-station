import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 24-12-2015.
 */

public class Controller extends UnicastRemoteObject implements ControllerInterface {
    Server server;

    public Controller(Server server) throws RemoteException {
        super();

        this.server=server;
        try {
            Registry registry = LocateRegistry.createRegistry(Constants.CONTROLLER_RMI_PORT);
            registry.bind(Constants.CONTROLLER_RMI_ID, this);

            System.out.println("Server ready");
        }
        catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }


    @Override
    public int login(String username, String password) throws RemoteException {
        return server.login(username, password);
    }

    @Override
    public int loginAsGuest() throws RemoteException {
        return server.loginAsGuest();
    }

    @Override
    public void logout(int userIdentifier) throws RemoteException {
        server.logout(userIdentifier);
    }

    @Override
    public int createNewMatch(int userIdentifier, String gameName) throws RemoteException {
        return server.createNewMatch(userIdentifier, gameName);
    }

    @Override
    public Vector getOpenGamesList() throws RemoteException {
        return server.getOpenGamesList();
    }

    @Override
    public ArrayList<AbstractMap.SimpleEntry<Integer, String>> getUserLastGames(int userIdentifier) throws RemoteException {
        return server.getUserLastGames(userIdentifier);
    }

    @Override
    public int joinOpenGame(int userIdentifier, String openGameText) throws RemoteException {
        return server.joinOpenGame(userIdentifier, openGameText);
    }

    @Override
    public int getGamePort(int gameIdentifier) throws RemoteException {
        return server.getGamePort(gameIdentifier);
    }

    @Override
    public int startGameReplay(int gameDatabaseID) throws RemoteException {
        return server.startGameReplay(gameDatabaseID);
    }


}
