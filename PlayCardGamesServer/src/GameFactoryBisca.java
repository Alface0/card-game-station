/**
 * Created by daniel on 25-12-2015.
 */
public class GameFactoryBisca extends GameFactory {

    @Override
    public GameRules getGameRules(Game game) {
        return new BiscaRules(game);
    }

    @Override
    public GameIA getGameIA() {
        return new BiscaIA();
    }

    public Deck getGameDeck(int playersNumber) { return new BiscaDeck(playersNumber);}

    @Override
    public GameStartRules getGameStartRules() {
        return new BiscaStartRules();
    }


}
