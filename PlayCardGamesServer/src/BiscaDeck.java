import java.util.Random;

/**
 * Created by daniel on 05-03-2016.
 */
public class BiscaDeck extends Deck {
    public BiscaDeck(int playersNumber)
    {
        for(Constants.CardSuit suit : Constants.CardSuit.values())
        {
            for (String value : Constants.values.keySet()) {
                cards.add(new Card(suit, value));
            }
        }

        // When exists 3 players in bisca the deck will need to have one card removed
        if(playersNumber == 3)
        {
            // Lets remove one of the less valuable cards -> "2"
            Random random = new Random();
            Constants.CardSuit suit = Constants.CardSuit.values()[random.nextInt(Constants.CardSuit.values().length)];
            cards.remove(new Card(suit, "2"));
        }

        shuffleDeck();
    }
}
