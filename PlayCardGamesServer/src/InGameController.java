import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 11-02-2016.
 */
public class InGameController extends UnicastRemoteObject implements InGameControllerInterface {
    Manager gameManager;
    Server server;

    public InGameController(Manager gameManager, Server server) throws RemoteException {
        super();
        this.gameManager = gameManager;
        this.server = server;

        Registry registry = null;
        try {
            registry = LocateRegistry.createRegistry(gameManager.getGame().getGamePort());
            registry.bind(Constants.IN_GAME_CONTROLLER_RMI_ID, this);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void playCard(int userIdentifier, Card card) throws RemoteException {
        User user = server.getUserByIdentifier(userIdentifier);
        PlayCard playCard = new PlayCard(gameManager.getGame(), user, card);
        gameManager.play(playCard);
    }

    @Override
    public int getCurrentPlayerNumber() throws RemoteException {
        return gameManager.getCurrentPlayerNumber();
    }

    @Override
    public void returnToLobby(int userIdentifier) throws RemoteException {

    }

    @Override
    public int getCurrentTurnTime() throws RemoteException {
        return gameManager.getCurrentTurnTime();
    }

    @Override
    public ArrayList<String> getGamePlayersName() {
        return gameManager.getGamePlayersName();
    }

    @Override
    public String getCurrentPlayerName() throws RemoteException {
        return gameManager.getCurrentPlayerName();
    }

    @Override
    public int getDeckCardsNumber() throws RemoteException {
        return gameManager.getDeckCardsNumber();
    }

    @Override
    public ArrayList<Card> getMyHand(int gamePosition) throws RemoteException {
        return gameManager.getMyHand(gamePosition);
    }

    @Override
    public ArrayList<Integer> getPlayersCardCount() throws RemoteException {
        return gameManager.getPlayersCardCount();
    }

    @Override
    public Constants.CardSuit getGameTrumpSuit() throws RemoteException {
        return gameManager.getGameTrumpSuit();
    }

    @Override
    public ArrayList<AbstractMap.SimpleEntry<Card, String>> getTableCards() throws RemoteException {
        return gameManager.getTableCards();
    }

    @Override
    public boolean isGameFinished() {
        return gameManager.isGameFinished();
    }

    @Override
    public Vector getPlayersScoreList() throws RemoteException {
        return gameManager.getPlayersScoreList();
    }

    @Override
    public int getCurrentGameTurn() throws RemoteException {
        return gameManager.getCurrentGameTurn();
    }

    @Override
    public String getGameTime() throws RemoteException {
        return gameManager.getGameTime();
    }

    @Override
    public String getGameWinner() throws RemoteException {
        return gameManager.getGameWinner();
    }

    @Override
    public void returnFromGameToLobby(int userIdentifier) throws RemoteException {
        gameManager.returnFromGameToLobby(userIdentifier);
    }

    @Override
    public void moveGameBack() throws RemoteException {
        gameManager.moveGameBack();
    }

    @Override
    public void moveGameForward() throws RemoteException {
        gameManager.moveGameForward();
    }

    @Override
    public String getCurrentReplayMove() throws RemoteException {
        return gameManager.getCurrentReplayMove();
    }

    @Override
    public void returnFromReplayToLobby() throws RemoteException {
        gameManager.returnFromReplayToLobby();
    }
}
