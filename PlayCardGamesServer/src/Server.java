import java.rmi.RemoteException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by daniel on 24-12-2015.
 */
public class Server {
    ControllerInterface controller;
    InQueueController inQueueController;
    ArrayList<RegisteredUser> userList;
    ArrayList<User> loggedUserList;
    int nextUserID = 0;
    int nextBuilderID = 0;
    int nextGameID = 0;

    ArrayList<GameBuilder> gameBuilders;
    ArrayList<Game> games;

    public Server()
    {
        getRegisteredUsers();
        startController();
        startQueueController();
        gameBuilders = new ArrayList<GameBuilder>();
        games = new ArrayList<Game>();
    }

    private void startQueueController() {
        try {
            inQueueController = new InQueueController(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void startController()
    {
        try {
            controller = new Controller(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void getRegisteredUsers()
    {
        userList = RegisteredUser.GetRegisteredUsers();
        loggedUserList = new ArrayList<User>();
    }

    public int login(String username, String password)
    {
        for(RegisteredUser user : userList)
        {
            if(user.login(username, password))
            {
                if(loggedUserList.contains(user))
                {
                    System.out.println("Login failed: User " + user.getUsername() + " is already logged in");
                    return -2;
                }
                loggedUserList.add(user);
                System.out.println("Login success: User " + user.getUsername() + " logged in");
                user.loadLastPlayedGames();
                return user.identifier;
            }
        }
        return -1;
    }

    public int loginAsGuest() {
        int userIdentifier = generateNewUserIdentifier();
        String guestUsername = "Guest-" + userIdentifier;
        loggedUserList.add(new User(guestUsername, userIdentifier));

        return userIdentifier;
    }

    public void logout(int userIdentifier) {
        try
        {
            User userLoggingOut = getUserByIdentifier(userIdentifier);
            System.out.println(userLoggingOut.getUsername() + " logged out!");
            loggedUserList.remove(userLoggingOut);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    int generateNewUserIdentifier()
    {
        return ++nextUserID;
    }

    User getUserByIdentifier(int userIdentifier)
    {
        for(User user : loggedUserList)
        {
            if(user.identifier == userIdentifier)
                return user;
        }
        return null;
    }

    public Vector getOpenGamesList() {
        Vector vector = new Vector();
        for(GameBuilder gameBuilder : gameBuilders)
        {
            try {
                vector.add(gameBuilder.toString());
            } catch (Exception exception)
            {
                System.out.println("Error in getOpenGamesList while calling gameBuilder.toString()");
            }
        }
        return vector;
    }

    public ArrayList<AbstractMap.SimpleEntry<Integer, String>> getUserLastGames(int userIdentifier) {
        User user = getUserByIdentifier(userIdentifier);
        if(user instanceof RegisteredUser) {
            return ((RegisteredUser) user).getLastPlayedGames();
        }
        else
            return new ArrayList<>();
    }

    public int createNewMatch(int userIdentifier, String gameName) {
        try {
            GameBuilder gameBuilder = new GameBuilder(gameName, this);
            gameBuilders.add(gameBuilder);

            gameBuilder.addPlayer(userIdentifier);
            return gameBuilder.builderIdentifier;
        } catch (Exception exception)
        {
            return -1;
        }
    }

    public int generateNewGameIdentifier() {
        return ++nextGameID;
    }

    public int generateNewBuilderIdentifier()
    {
        return ++nextBuilderID;
    }

    public GameBuilder getGameBuilderByIdentifier(int gameBuilderIdentifier)
    {
        for(GameBuilder gameBuilder: gameBuilders)
        {
            if( gameBuilderIdentifier == gameBuilder.builderIdentifier)
                return gameBuilder;
        }
        return null;
    }

    public int getBuilderPlayersNumber(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        return gameBuilder.getPlayersNumber();
    }

    public int getBuilderMaxPlayersNumber(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        return gameBuilder.gameStartRules.getMaximumPlayersNumber();
    }

    public boolean isBuilderReady(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        return gameBuilder.canStartGame();
    }

    public Vector getBuilderPlayersList(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        Vector vector = new Vector();
        for(User user: gameBuilder.humanPlayers)
            vector.add(user.getUsername());

        for(String level: gameBuilder.botPlayers)
            vector.add(gameBuilder.gameName + " " + level + " Bot");

        return vector;
    }

    public int joinOpenGame(int userIdentifier, String openGameText) {
        try{
            String [] gameTextTokens = openGameText.split("#");
            int gameIdentifier = Integer.parseInt(gameTextTokens[0]);
            GameBuilder gameBuilder = getGameBuilderByIdentifier(gameIdentifier);
            if(gameBuilder.getPlayersNumber() < gameBuilder.gameStartRules.getMaximumPlayersNumber())
            {
                gameBuilder.addPlayer(userIdentifier);
                return gameBuilder.builderIdentifier;
            }
            else
            {
                return -2;
            }
        } catch (Exception exception)
        {
            return -1;
        }
    }

    public String getRoomName(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        return gameBuilder.toString();
    }

    public void returnToLobby(int builderIdentifier, int userIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        User user = getUserByIdentifier(userIdentifier);

        gameBuilder.humanPlayers.remove(user);

        if(gameBuilder.humanPlayers.isEmpty())
            gameBuilders.remove(gameBuilder);
    }

    public boolean addBot(int builderIdentifier, String level) {
            GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
            return gameBuilder.addBot(level);
    }

    public int startGame(int builderIdentifier, int userIdentifier) {
        try{
            GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
            if(gameBuilder.canStartGame()) {
                Game game = new Game(gameBuilder);
                gameBuilder.humanPlayers.remove(getUserByIdentifier(userIdentifier));
                gameBuilder.botPlayers.removeAll(gameBuilder.botPlayers);
                gameBuilder.gameIdentifier = game.gameIdentifier;
                games.add(game);

                removeBuilderIfEmpty(gameBuilder);

                return game.gameIdentifier;
            }
            else
            {
                return -2;
            }

        } catch (Exception exception)

        {
            System.out.println("Start Game Failed -> " + exception.toString());
            return -1;
        }

    }

    private void removeBuilderIfEmpty(final GameBuilder gameBuilder) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                    if(gameBuilder.humanPlayers.isEmpty())
                    {
                        gameBuilders.remove(gameBuilder);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public String getGameName(int builderIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        return gameBuilder.gameName;
    }

    public int isGameStarted(int builderIdentifier, int userIdentifier) {
        GameBuilder gameBuilder = getGameBuilderByIdentifier(builderIdentifier);
        int gameIdentifier = gameBuilder.gameIdentifier;

        if(gameBuilder.isGameStarted())
        {
            gameBuilder.humanPlayers.remove(getUserByIdentifier(userIdentifier));

            return gameIdentifier;
        }

        return -1;
    }

    private Game getGameByIdentifier(int gameIdentifier) {
        for(Game game : games)
        {
            if(game.gameIdentifier == gameIdentifier)
                return game;
        }

        return null;
    }

    public int getGamePort(int gameIdentifier) {
        return getGameByIdentifier(gameIdentifier).getGamePort();
    }


    public int startGameReplay(int gameDatabaseID) {
        Game game = new Game(this, gameDatabaseID);
        games.add(game);
        return game.gameIdentifier;
    }
}
