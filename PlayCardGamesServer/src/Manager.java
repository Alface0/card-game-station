import java.rmi.RemoteException;
import java.util.*;

/**
 * Created by daniel on 29-12-2015.
 */
public class Manager extends Thread {
    InGameController inGameController;
    Game game;
    Server server;
    ArrayList<GameMove> gameMoves;
    int currentGameMove;
    boolean isTimeCounting;

    public Manager(Game game, Server server)
    {
        isTimeCounting = true;
        this.game = game;
        this.server = server;
        gameMoves = new ArrayList<GameMove>();
        currentGameMove = 0;
        try {
            inGameController = new InGameController(this, server);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void run()
    {
        startTimeCounter();
        long nextTime = System.currentTimeMillis() + 1000;
        while (!game.isFinished())
        {
            long currentTime = System.currentTimeMillis();
            if(currentTime >= nextTime && isTimeCounting ) {
                checkPlayer();
                nextTime = currentTime + 1000;
            }
        }
    }

    private void checkPlayer() {
        User user = getCurrentPlayer();
        if(user instanceof GameBot)
        {
            GameMove gameMove = ((GameBot) user).play();
            play(gameMove);
        }
    }

    public void startTimeCounter()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long nextSecondTime = System.currentTimeMillis() + 1000;
                while (!game.isFinished())
                {
                    long currentTime = System.currentTimeMillis();
                    if(isTimeCounting)
                    {
                        if(currentTime >= nextSecondTime)
                        {
                            if(game.currentTurnTime > 0)
                            {
                                game.currentTurnTime--;
                                game.totalTime++;
                                nextSecondTime = System.currentTimeMillis() + 1000;
                            }
                            else
                            {
                                isTimeCounting = false;
                                playRandom();
                            }
                        }
                    }
                }
            }
        }).start();
    }

    private void playRandom() {
        Random random = new Random();
        User currentPlayer = game.orderedPlayersList.get(game.currentPlayer);
        ArrayList<Card> currentPlayerHand = game.playersHands.get(currentPlayer);
        PlayCard playCard = new PlayCard(game, currentPlayer, currentPlayerHand.get(random.nextInt(currentPlayerHand.size())));
        play(playCard);
    }

    public void changeTurn()
    {
        game.currentPlayer++;
        game.resetTurnTimer();
        if(game.currentPlayer >= game.orderedPlayersList.size()) {
            game.currentPlayer = 0;
        }

        if(game.currentPlayer == game.turnFirstPlayer) {
            game.defineTurnWinner();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    long nextSecondTime = System.currentTimeMillis() + 2000;
                    while (System.currentTimeMillis() < nextSecondTime )
                        continue;
                    /*
                    ClearTable clearTable = new ClearTable();
                    clearTable.execute();
                    gameMoves.add(clearTable);
                    */
                    game.clearTable();
                    if(game.gameRules.isGameFinished())
                    {
                        System.out.println("O jogo terminou!");
                        game.finished = true;
                        game.saveGame();
                    }
                    else
                    {
                        isTimeCounting = true;
                        game.currentTurn++;
                        giveTurnCards();
                    }
                }
            }).start();
        }
        else
        {
            isTimeCounting = true;
        }

    }

    public void play(GameMove gameMove) {
        if(game.gameRules.canPlay(gameMove)) {
            gameMoves.add(gameMove);
            gameMove.execute();
            isTimeCounting = false;
            changeTurn();
        }
        else
        {
            System.out.println("Rejected -> " + gameMove.toString());
        }
    }

    public Game getGame() {
        return game;
    }

    public User getCurrentPlayer() {
        return game.orderedPlayersList.get(game.currentPlayer);
    }

    public int getCurrentPlayerNumber() {
        return game.currentPlayer;
    }

    public void giveTurnCards() {
        for(User user: game.orderedPlayersList)
        {
            giveCard(user);
        }
    }

    public void giveInitialCards()
    {
        for(User user: game.orderedPlayersList)
        {
            for (int i = 0; i < game.gameRules.getInitialCardsNumber(); i++)
            {
                giveCard(user);
            }
        }
    }

    public int getCurrentTurnTime() {
        return game.currentTurnTime;
    }

    public ArrayList<String> getGamePlayersName() {
        ArrayList<String> nomesPlayers = new ArrayList<String>();
        for(User user: game.orderedPlayersList)
        {
            nomesPlayers.add(user.getUsername());
        }
        return nomesPlayers;
    }

    public String getCurrentPlayerName() {
        return getCurrentPlayer().getUsername();
    }

    public int getDeckCardsNumber() {
        return game.deck.cards.size();
    }

    public ArrayList<Card> getMyHand(int gamePosition) {
        return game.playersHands.get(game.orderedPlayersList.get(gamePosition));
    }

    public ArrayList<Integer> getPlayersCardCount() {
        ArrayList<Integer> playersCardCount = new ArrayList<Integer>();
        for(ArrayList<Card> hand: game.playersHands.values())
        {
            playersCardCount.add(hand.size());
        }
        return playersCardCount;
    }

    public Constants.CardSuit getGameTrumpSuit() {
        return game.trumpSuit;
    }

    public synchronized ArrayList<AbstractMap.SimpleEntry<Card, String>> getTableCards()
    {
        ArrayList<AbstractMap.SimpleEntry<Card, String>> tableCards = new ArrayList<AbstractMap.SimpleEntry<Card, String>>();
        for(AbstractMap.SimpleEntry<User, Card> pair: game.table)
            tableCards.add(new AbstractMap.SimpleEntry<Card, String>(pair.getValue(), pair.getKey().username));

        return tableCards;
    }

    public void giveCard(User user)
    {
        if(!game.deck.cards.isEmpty())
        {
            Card card = game.deck.cards.get(0);
            GiveCard giveCard = new GiveCard(card, game, user);
            giveCard.execute();
            gameMoves.add(giveCard);
        }
    }

    public boolean isGameFinished() {
        return game.isFinished();
    }

    public Vector getPlayersScoreList() {
        Vector<String> vector = new Vector<String>();
        vector.add("Player : Score");
        for(Map.Entry<User, Integer> player: game.playersScores.entrySet())
        {
            vector.add(player.getKey().getUsername() + " : " + player.getValue());
        }

        return vector;
    }

    public int getCurrentGameTurn() {
        return game.currentTurn;
    }

    public String getGameTime() {
        int minutes = 0;
        int seconds = 0;

        try {
            minutes = game.totalTime / 60;
            seconds = game.totalTime % 60;
        } catch (Exception exception)
        {
            //Ignore
        }

        return String.format("%02d", minutes) + "m " +  String.format("%02d", seconds) + "s";
    }



    public String getGameWinner() {
        User user = game.getWinner();
        String username = "";
        try {
            username = user.getUsername();
        } catch (Exception exception)
        {
            //Ignore, if the user is null the string is supposed to got empty
        }

        return username;
    }

    public void returnFromGameToLobby(int userIdentifier) {
        game.usersInGame.remove(server.getUserByIdentifier(userIdentifier));
        if(game.usersInGame.isEmpty())
            server.games.remove(game);
    }

    public void moveGameBack() {
        if(!gameMoves.isEmpty())
        {
            GameMove gameMove = gameMoves.get(currentGameMove);
            gameMove.undo();
            currentGameMove--;
        }
    }

    public void moveGameForward() {
        if(currentGameMove < (gameMoves.size() - 1))
        {
            GameMove gameMove = gameMoves.get(currentGameMove);
            gameMove.execute();
            currentGameMove++;
        }
    }


    public String getCurrentReplayMove() {
        return currentGameMove + "/" + (gameMoves.size() - 1);
    }

    public void returnFromReplayToLobby() {
        server.games.remove(game);
    }
}
