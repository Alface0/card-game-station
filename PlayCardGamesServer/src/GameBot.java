/**
 * Created by daniel on 27-02-2016.
 */
public interface GameBot {
    GameMove play();
    Card selectCardToPlay();
}
