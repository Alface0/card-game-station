import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by daniel on 26-12-2015.
 */
public class RegisteredUser extends User{
    private String password;
    ArrayList<AbstractMap.SimpleEntry<Integer, String>> playedGames;


    public RegisteredUser(String username, String password, int identifier) {
        super(username, identifier);
        this.password = password;
        playedGames = new ArrayList<>();
    }

    public boolean login(String username, String password)
    {
        if(this.username.equals(username) && this.password.equals(password))
            return true;
        else
            return false;
    }


    /*

    Persistance Logic bellow this point

     */

    public int getUserDatabaseID()
    {
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        int ID = -1;

        String sqlQuery = "SELECT ID FROM USER WHERE USERNAME='" + username + "'";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);

        try {
            resultSet.next();
            ID = new Integer(resultSet.getInt("ID"));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sqLiteJDBC.closeConnection();
        return ID;
    }

    public void loadLastPlayedGames()
    {
        playedGames = new ArrayList<>();
        int userID = getUserDatabaseID();
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();

        String sqlQuery = "SELECT GAME FROM USERGAME WHERE USER=" + userID + ";";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);

        try {
            while(resultSet.next())
            {
                int gameID = resultSet.getInt("GAME");

                String gameSqlQuery = "SELECT NAME FROM GAME WHERE ID=" + gameID + ";";
                ResultSet gameResultSet = sqLiteJDBC.executeQuery(gameSqlQuery);

                while(gameResultSet.next()) {
                    String gameName = gameResultSet.getString("NAME");

                    playedGames.add(new AbstractMap.SimpleEntry<Integer, String>(gameID, gameName));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<AbstractMap.SimpleEntry<Integer, String>> getLastPlayedGames()
    {
        int i=0;
        ArrayList<AbstractMap.SimpleEntry<Integer, String>> lastPlayedGames = new ArrayList<>();
        for(AbstractMap.SimpleEntry<Integer, String> game: playedGames)
        {
            lastPlayedGames.add(game);
            if(i >= 10)
                break;
        }
        return lastPlayedGames;
    }
}
