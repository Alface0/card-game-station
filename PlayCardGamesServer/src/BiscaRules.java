import java.util.AbstractMap;

/**
 * Created by daniel on 28-12-2015.
 */
public class BiscaRules implements GameRules {
    Game game;

    public BiscaRules(Game game)
    {
        this.game = game;
    }

    @Override
    public boolean isGameFinished() {
        for(User user: game.playersHands.keySet())
        {
            if(game.playersHands.get(user).size() > 0)
                return false;
        }
        return true;
    }

    public boolean isPlayerTurn(User user)
    {
        if(game.gameManager.getCurrentPlayer().equals(user))
            return true;

        return false;
    }

    public boolean isCardValid(Card card, User user)
    {
        Card firstCard;
        try
        {
            firstCard = game.table.get(0).getValue();
        }
        catch (Exception exception)
        {
            // Since the first card is not available it means the current card is the first
            return true;
        }

        //If the card is of the suit of the first card, then the card can be played
        if(card.suit == firstCard.suit)
            return true;

        //If the player doesn't have any card of the first card suit then the user can play any card
        boolean hasSuitCard = true;
        for(Card handCard : game.playersHands.get(user))
        {
            if(handCard.suit == firstCard.suit)
                hasSuitCard = false;
        }

        return hasSuitCard;
    }

    public boolean isLastThreeMoves()
    {
        return game.deck.cards.isEmpty();
    }

    @Override
    public boolean canPlay(GameMove gameMove) {
        User user = gameMove.getUser();
        if(isPlayerTurn(user)) {
            if(isLastThreeMoves()) {
                if (isCardValid(gameMove.getCard(), user)) {
                    return true;
                }
            }
            else
            {
                if(!isGameFinished())
                {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public int getTurnTime() {
        return 60;
    }

    @Override
    public int getInitialCardsNumber() {
        return 3;
    }

    @Override
    public boolean gameHasTrumpSuit() {
        return true;
    }

    @Override
    public User defineTurnWinner() {
        AbstractMap.SimpleEntry<User, Card> winner = null;
        int i = 0;
        for(AbstractMap.SimpleEntry<User, Card> pair: game.table)
        {
            if(i==0)
            {
                winner=pair;
                i++;
            }
            else
            {
                if (isTrump(winner.getValue().suit))
                {
                    if (isTrump(pair.getValue().suit) && pair.getValue().getNumericValue() > winner.getValue().getNumericValue())
                        winner = pair;
                }
                else
                {
                    if (isTrump(pair.getValue().suit) || (pair.getValue().getNumericValue() > winner.getValue().getNumericValue() && pair.getValue().suit == winner.getValue().suit))
                        winner = pair;
                }
            }
        }

        return winner.getKey();
    }

    boolean isTrump(Constants.CardSuit suit)
    {
        if(suit == game.trumpSuit)
            return true;

        return false;
    }

}
