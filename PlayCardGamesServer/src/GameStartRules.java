/**
 * Created by daniel on 18-02-2016.
 */
public interface GameStartRules {
    public int getMinimumPlayersNumber();
    public int getMaximumPlayersNumber();
}
