/**
 * Created by daniel on 06-03-2016.
 */
public class CreateDatabase {
    static SQLiteJDBC sqLiteJDBC;

    public CreateDatabase()
    {
        sqLiteJDBC = new SQLiteJDBC();
        turnForeignKeysOn();
        createUserTable();
        createRegisteredUserTable();
        createGameTypeTable();
        createGameTable();
        createMoveTypeTable();
        createUserGameTable();
        createGameMoveTable();
        sqLiteJDBC.closeConnection();
    }


    private static void turnForeignKeysOn() {
        String sql = "pragma foreign_keys=on;";
        sqLiteJDBC.executeUpdate(sql);
    }

    public static void createUserTable()
    {
        String sql = "CREATE TABLE USER " +
                "(ID INTEGER PRIMARY KEY  AUTOINCREMENT," +
                " USERNAME       TEXT    NOT NULL," +
                " TYPE TEXT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

    private static void createRegisteredUserTable() {
        String sql = "CREATE TABLE REGISTEREDUSER " +
                "(ID INTEGER PRIMARY KEY  AUTOINCREMENT," +
                " USER INT REFERENCES USER(ID) NOT NULL," +
                " PASSWORD       TEXT    NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

    public static void createGameTypeTable()
    {
        String sql = "CREATE TABLE GAMETYPE " +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " TYPE TEXT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

    public static void createGameTable()
    {
        String sql = "CREATE TABLE GAME " +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " TYPE INT REFERENCES GAMETYPE(ID) NOT NULL," +
                " TRUMP TEXT NOT NULL," +
                " NAME TEXT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

    private static void createMoveTypeTable()
    {
        String sql = "CREATE TABLE MOVETYPE " +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " TYPE TEXT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

    private static void createUserGameTable() {
        String sql = "CREATE TABLE USERGAME " +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " USER INT REFERENCES USER(ID) NOT NULL," +
                " GAME INT REFERENCES GAME(ID) NOT NULL," +
                " SCORE INT NOT NULL," +
                " POSITION INT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }


    private static void createGameMoveTable()
    {
        String sql = "CREATE TABLE GAMEMOVE " +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " GAME INT REFERENCES GAME(ID) NOT NULL," +
                " USER INT REFERENCES USER(ID) NOT NULL," +
                " MOVETYPE INT REFERENCES MOVETYPE(ID)," +
                " CARDVALUE TEXT NOT NULL," +
                " CARDSUIT TEXT NOT NULL)";
        sqLiteJDBC.executeUpdate(sql);
    }

}
