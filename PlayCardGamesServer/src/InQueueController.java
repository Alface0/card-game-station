import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

/**
 * Created by daniel on 21-02-2016.
 */
public class InQueueController extends UnicastRemoteObject implements InQueueControllerInterface {
    Server server;

    public InQueueController(Server server) throws RemoteException
    {
        super();
        this.server = server;

        try {
            Registry registry = LocateRegistry.createRegistry(Constants.IN_QUEUE_CONTROLLER_RMI_PORT);
            registry.bind(Constants.IN_QUEUE_CONTROLLER_RMI_ID, this);

            System.out.println("InQueue Controller Registered!");
        }
        catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public Vector getPlayersList(int builderIdentifier) throws RemoteException {
        return server.getBuilderPlayersList(builderIdentifier);
    }

    @Override
    public boolean addBot(int builderIdentifier, String level) throws RemoteException {
        return server.addBot(builderIdentifier, level);
    }

    @Override
    public void returnToLobby(int builderIdentifier, int userIdentifier) throws RemoteException {
        server.returnToLobby(builderIdentifier, userIdentifier);
    }

    @Override
    public String getRoomName(int builderIdentifier) throws RemoteException {
        return server.getRoomName(builderIdentifier);
    }

    @Override
    public String getGameName(int builderIdentifier) throws RemoteException {
        return server.getGameName(builderIdentifier);
    }

    @Override
    public int getPlayersNumber(int builderIdentifier) throws RemoteException {
        return server.getBuilderPlayersNumber(builderIdentifier);
    }

    @Override
    public int getMaxPlayersNumber(int builderIdentifier) throws RemoteException {
        return server.getBuilderMaxPlayersNumber(builderIdentifier);
    }

    @Override
    public boolean canStartGame(int builderIdentifier) throws RemoteException {
        return server.isBuilderReady(builderIdentifier);
    }

    @Override
    public int startGame(int builderIdentifier, int userIdentifier) throws RemoteException {
        return server.startGame(builderIdentifier, userIdentifier);
    }

    @Override
    public int isGameStarted(int builderIdentifier, int userIdentifier) throws RemoteException {
        return server.isGameStarted(builderIdentifier, userIdentifier);
    }
}
