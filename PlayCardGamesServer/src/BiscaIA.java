import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by daniel on 28-12-2015.
 */
public class BiscaIA implements GameIA {

    @Override
    public User getBot(Game game, String level) {
        if(level.equals("Easy")) {
            return new BiscaEasyBot(game);
        }

        // Dificulty level not implemented
        throw new NotImplementedException();
    }

}
