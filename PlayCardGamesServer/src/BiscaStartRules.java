/**
 * Created by daniel on 18-02-2016.
 */
public class BiscaStartRules implements GameStartRules {
    @Override
    public int getMinimumPlayersNumber() {
        return 2;
    }

    @Override
    public int getMaximumPlayersNumber() {
        return 3;
    }
}
