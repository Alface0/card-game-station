/**
 * Created by daniel on 25-12-2015.
 */
public interface GameRules {
    boolean isGameFinished();
    boolean canPlay(GameMove gameMove);
    int getTurnTime();
    int getInitialCardsNumber();
    boolean gameHasTrumpSuit();
    User defineTurnWinner();
}
