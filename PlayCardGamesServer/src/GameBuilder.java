import java.util.ArrayList;

/**
 * Created by daniel on 18-02-2016.
 */
public class GameBuilder {
    int builderIdentifier;
    String gameName;
    ArrayList<User> humanPlayers;
    ArrayList<String> botPlayers;
    Server server;
    GameStartRules gameStartRules;
    int gameIdentifier;

    public GameBuilder(String gameName, Server server)
    {
        this.builderIdentifier = server.generateNewBuilderIdentifier();
        this.gameName = gameName;
        this.server = server;
        gameStartRules = GameFactory.getGame(gameName).getGameStartRules();
        humanPlayers = new ArrayList<User>();
        botPlayers = new ArrayList<String>();
        gameIdentifier = -1;
    }

    public void addPlayer(int userIdentifier)
    {
        User user = server.getUserByIdentifier(userIdentifier);
        if(user != null && getPlayersNumber() < gameStartRules.getMaximumPlayersNumber())
        {
            humanPlayers.add(user);
        }
    }

    public boolean addBot(String level)
    {
        if(getPlayersNumber() < gameStartRules.getMaximumPlayersNumber())
        {
            botPlayers.add(level);
            return true;
        }

        return false;
    }


    public boolean canStartGame()
    {
        if(getPlayersNumber() >= gameStartRules.getMinimumPlayersNumber() && getPlayersNumber() <= gameStartRules.getMaximumPlayersNumber())
            return true;
        else
            return false;
    }

    public int getPlayersNumber()
    {
        return humanPlayers.size() + botPlayers.size();
    }

    public Game getGame()
    {
        if(canStartGame())
            return new Game(this);
        else
            return null;
    }

    public String toString()
    {
        String msg = "";
        for(User user: humanPlayers)
        {
            msg = msg + user.toString();
        }
        return  builderIdentifier + "#Game Name - " + gameName + " # " + getPlayersNumber() + " / " + gameStartRules.getMaximumPlayersNumber();
    }

    public boolean isGameStarted()
    {
        if(gameIdentifier >= 0)
        {
            return true;
        }

        return false;
    }

}
