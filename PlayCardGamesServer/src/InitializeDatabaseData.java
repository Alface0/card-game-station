/**
 * Created by daniel on 06-03-2016.
 */
public class InitializeDatabaseData {
    static String[] registeredUsers = new String[]{"Admin", "Admin"
                                            , "Daniel", "123asd"
                                            , "Solange", "123asd"};
    static String[] moveTypes = new String[]{"PlayCard", "GiveCard"};

    public InitializeDatabaseData()
    {
        createUsers();
        createMoveTypes();
        createGameTypes();
    }

    public static void createUsers()
    {
        int i=0;
        while(i < registeredUsers.length)
        {
            User.CreateRegisteredUser(registeredUsers[i++], registeredUsers[i++]);
        }
    }

    public static void createMoveTypes()
    {
        String sqlQuery;
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        // Clear MoveType table
        sqlQuery = "DELETE FROM MOVETYPE;";
        sqLiteJDBC.executeUpdate(sqlQuery);

        // Register the existing GameMoves
        for(String moveType: moveTypes)
        {
            sqlQuery = "INSERT INTO MOVETYPE (TYPE) VALUES ('" + moveType + "');";
            sqLiteJDBC.executeUpdate(sqlQuery);
        }

        sqLiteJDBC.closeConnection();
    }

    public static void createGameTypes()
    {
        String sqlQuery;
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        // Clear MoveType table
        sqlQuery = "DELETE FROM GAMETYPE;";
        sqLiteJDBC.executeUpdate(sqlQuery);

        // Register the existing GameMoves
        for(String game: Constants.GAMELIST)
        {
            sqlQuery = "INSERT INTO GAMETYPE (TYPE) VALUES ('" + game + "');";
            sqLiteJDBC.executeUpdate(sqlQuery);
        }

        sqLiteJDBC.closeConnection();
    }
}
