import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by daniel on 28-12-2015.
 */
public abstract class Deck {
    ArrayList<Card> cards;
    public Deck()
    {
        cards = new ArrayList<Card>();
    }

    public void shuffleDeck()
    {
        Collections.shuffle(cards);
    }

    public String toString()
    {
        String mensagem = "";
        for(Card card : cards)
        {
            mensagem += card.toString() + "\n";
        }

        return mensagem;
    }

    public static Constants.CardSuit stringToCardSuit(String string)
    {
        if(string.equals("PAUS"))
            return Constants.CardSuit.PAUS;
        if(string.equals("COPAS"))
            return Constants.CardSuit.COPAS;
        if(string.equals("ESPADAS"))
            return Constants.CardSuit.ESPADAS;
        if(string.equals("OUROS"))
            return Constants.CardSuit.OUROS;

        return null;
    }

}
