import java.util.ArrayList;
import java.util.Random;

/**
 * Created by daniel on 04-01-2016.
 */
public class BiscaEasyBot extends User implements GameBot {
    Game game;
    public BiscaEasyBot(Game game) {
        this.game = game;
        this.identifier = game.getNewBotIdentifier();
        username = "BiscaBot-Easy#" + identifier;
    }

    @Override
    public GameMove play() {
        Card card = selectCardToPlay();
        GameMove gameMove = new PlayCard(game, this, card);
        return gameMove;
    }

    @Override
    public Card selectCardToPlay() {
        Random random = new Random();
        ArrayList<Card> hand = game.playersHands.get(this);
        Card card = hand.get(random.nextInt(hand.size()));
        return card;
    }


}
