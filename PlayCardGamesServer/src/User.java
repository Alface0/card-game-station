import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by daniel on 25-12-2015.
 */
// Guest
public class User {
    String username;
    int identifier;

    public User(){ }

    public User(String username, int identifier)
    {
        this.username = username;
        this.identifier = identifier;

    }

    public void setIdentifier(int identifier)
    {
        this.identifier = identifier;
    }

    public String getUsername() {
        return username;
    }


    /*

    ////// PERSISTANT LOGIC BELOW THIS POINT

*/
    public static int CreateUser(String username, String type) {
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        String sqlStatement = "INSERT INTO USER (USERNAME, TYPE) VALUES ('" + username + "', '" + type + "');";

        try {
            Statement statement = sqLiteJDBC.connection.createStatement();
            statement.executeUpdate(sqlStatement);

            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();

            int id = resultSet.getInt(1);
            statement.close();
            sqLiteJDBC.closeConnection();
            return id;
        } catch (Exception exception)
        {
            System.out.println("Failed when inserting user in the database: " + exception.toString() );
        }

        sqLiteJDBC.closeConnection();
        return -1;
    }

    public static void CreateRegisteredUser(String username, String password)
    {
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        String sqlStatement;

        int userID = CreateUser(username, "RegisteredUser");

        sqlStatement = "INSERT INTO REGISTEREDUSER (USER, PASSWORD) VALUES('" + userID + "', '" + password + "');";
        sqLiteJDBC.executeUpdate(sqlStatement);

        sqLiteJDBC.closeConnection();
    }

    public static ArrayList<RegisteredUser> GetRegisteredUsers()
    {
        ArrayList<RegisteredUser> registeredUsers = new ArrayList<>();
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        String userSqlStatement = "SELECT ID, USERNAME FROM USER WHERE TYPE='RegisteredUser';";
        ResultSet userResultSet = sqLiteJDBC.executeQuery(userSqlStatement);

        try {
            while (userResultSet.next())
            {
                int id = userResultSet.getInt("ID");
                String username = userResultSet.getString("USERNAME");

                String sqlStatement = "SELECT PASSWORD FROM REGISTEREDUSER WHERE USER=" + id + ";";
                ResultSet resultSet = sqLiteJDBC.executeQuery(sqlStatement);
                resultSet.next();
                String password = resultSet.getString("PASSWORD");

                registeredUsers.add(new RegisteredUser(username, password, id));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        sqLiteJDBC.closeConnection();

        return registeredUsers;
    }

    public static User getUser(int userDatabaseID)
    {
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        String sqlQuery = "SELECT * FROM USER WHERE ID=" + userDatabaseID + ";";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);

        int userID = -1;
        String name = "";
        try {
            while (resultSet.next())
            {
                userID = resultSet.getInt("ID");
                name = resultSet.getString("USERNAME");
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new User(name, userID);
    }

}
