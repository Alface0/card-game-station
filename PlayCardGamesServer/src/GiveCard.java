/**
 * Created by daniel on 07-03-2016.
 */
public class GiveCard implements GameMove {
    Card card;
    Game game;
    User user;

    public GiveCard(Card card, Game game, User user)
    {
        this.card = card;
        this.game = game;
        this.user = user;
    }

    @Override
    public void execute() {
        game.playersHands.get(user).add(card);
        game.deck.cards.remove(card);
    }

    @Override
    public void undo() {
        game.deck.cards.add(card);
        game.playersHands.get(user).remove(card);
    }

    @Override
    public Card getCard() {
        return card;
    }

    @Override
    public User getUser() {
        return user;
    }
}
