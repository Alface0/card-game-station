import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created by daniel on 28-12-2015.
 */
public class Game {
    int gameIdentifier;
    int botCount;
    String gameType;
    String gameName;
    GameFactory gameFactory;
    HashMap<User, ArrayList<Card>> playersHands;
    HashMap<User, Integer> playersScores;
    ArrayList<User> orderedPlayersList;
    ArrayList<User> usersInGame;
    Deck deck;
    ArrayList<AbstractMap.SimpleEntry<User, Card>> table;
    GameRules gameRules;
    GameIA gameIA;
    Manager gameManager;
    int currentPlayer;
    int turnFirstPlayer;
    int currentTurn;
    int totalTime;
    int currentTurnTime;
    volatile boolean finished;
    Constants.CardSuit trumpSuit;

    public Game(GameBuilder gameBuilder) {
        gameIdentifier = gameBuilder.server.generateNewGameIdentifier();
        finished = false;
        this.gameType = gameBuilder.gameName;
        playersHands = new HashMap<User, ArrayList<Card>>();
        playersScores = new HashMap<User, Integer>();
        orderedPlayersList = new ArrayList<User>();
        gameFactory = GameFactory.getGame(gameBuilder.gameName);
        gameIA = gameFactory.getGameIA();
        gameRules = gameFactory.getGameRules(this);
        gameManager = new Manager(this, gameBuilder.server);
        usersInGame = new ArrayList<User>();
        table = new ArrayList<AbstractMap.SimpleEntry<User, Card>>();
        botCount = 0;
        currentPlayer = 0;
        turnFirstPlayer = 0;
        currentTurn = 0;
        totalTime = 0;
        currentTurnTime = gameRules.getTurnTime();
        if (gameRules.gameHasTrumpSuit()) {
            Constants.CardSuit[] cardSuits = Constants.CardSuit.values();
            Random random = new Random();
            trumpSuit = cardSuits[random.nextInt(cardSuits.length)];
        }

        for (User user : gameBuilder.humanPlayers) {
            playersHands.put(user, new ArrayList<Card>());
        }

        for (String botLevel : gameBuilder.botPlayers) {
            addBot(botLevel);
        }


        for (User user : playersHands.keySet()) {
            orderedPlayersList.add(user);
            playersScores.put(user, 0);
            usersInGame.add(user);
        }

        deck = gameFactory.getGameDeck(playersHands.size());
        gameManager.giveInitialCards();
        gameManager.start();
        Date date = new Date();
        gameName = gameType + " " + String.format("%02d", date.getHours()) + ":" + String.format("%02d", date.getMinutes()) + " " + String.format("%02d", date.getDay()) + "." + String.format("%02d", date.getMonth()) + "." + (date.getYear() + 1900);
    }

    public Game(Server server, int gameDatabaseID)
    {
        gameIdentifier = server.generateNewGameIdentifier();
        finished = false;
        playersHands = new HashMap<User, ArrayList<Card>>();
        playersScores = new HashMap<User, Integer>();
        usersInGame = new ArrayList<User>();
        table = new ArrayList<AbstractMap.SimpleEntry<User, Card>>();
        botCount = 0;
        currentPlayer = 0;
        turnFirstPlayer = 0;
        currentTurn = 0;
        totalTime = 0;

        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        String sqlQuery = "SELECT * FROM GAME WHERE ID=" + gameDatabaseID + ";";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);

        String databaseGameSuit = "";
        String databaseGameType = "";
        String databaseGameName = "";
        orderedPlayersList = new ArrayList<User>();
        try {
            while (resultSet.next())
            {
                // Get Game Data
                int gameTypeID = resultSet.getInt("TYPE");
                databaseGameSuit = resultSet.getString("TRUMP");
                databaseGameName = resultSet.getString("NAME");

                String gameTypeSqlQuery = "SELECT TYPE FROM GAMETYPE WHERE ID=" + gameTypeID + ";";
                ResultSet gameTypeResultSet = sqLiteJDBC.executeQuery(gameTypeSqlQuery);


                while(gameTypeResultSet.next())
                {
                    databaseGameType = gameTypeResultSet.getString("TYPE");
                }

                gameType = databaseGameType;
                gameName = databaseGameName;
                trumpSuit = Deck.stringToCardSuit(databaseGameSuit);

                gameFactory = GameFactory.getGame(gameType);
                gameIA = gameFactory.getGameIA();
                gameRules = gameFactory.getGameRules(this);
                gameManager = new Manager(this, server);

                // Get game players Data
                String userGameSqlQuery = "SELECT * FROM USERGAME WHERE GAME=" + gameDatabaseID + ";";
                ResultSet userGameResultSet = sqLiteJDBC.executeQuery(userGameSqlQuery);

                while(userGameResultSet.next())
                {
                    int userID = userGameResultSet.getInt("USER");
                    orderedPlayersList.add(User.getUser(userID));
                }

                for (User user : orderedPlayersList) {
                    playersHands.put(user, new ArrayList<Card>());
                    playersScores.put(user, 0);
                    usersInGame.add(user);
                }

                //Get GameMoves data
                String gameMoveSqlQuery = "SELECT * FROM GAMEMOVE WHERE GAME=" + gameDatabaseID + ";";
                ResultSet gameMoveResultSet = sqLiteJDBC.executeQuery(gameMoveSqlQuery);

                while(gameMoveResultSet.next())
                {
                    int userID = gameMoveResultSet.getInt("USER");
                    User currentUser = null;
                    for(User user : orderedPlayersList)
                    {
                        if(userID == user.identifier)
                            currentUser = user;
                    }

                    String databaseCardSuit = gameMoveResultSet.getString("CARDSUIT");
                    String cardValue = gameMoveResultSet.getString("CARDVALUE");
                    int moveTypeID = gameMoveResultSet.getInt("MOVETYPE");

                    Constants.CardSuit cardSuit = Deck.stringToCardSuit(databaseCardSuit);

                    String moveTypeSqlQuery = "SELECT TYPE FROM MOVETYPE WHERE ID=" + moveTypeID + ";";
                    ResultSet moveTypeResultSet = sqLiteJDBC.executeQuery(moveTypeSqlQuery);

                    GameMove gameMove = null;
                    String stringGameMove = "";
                    while (moveTypeResultSet.next())
                    {
                        stringGameMove = moveTypeResultSet.getString("TYPE");
                    }

                    Card card = new Card(cardSuit, cardValue);
                    gameMove = getStringAsGameMove(stringGameMove, currentUser, card);
                    gameManager.gameMoves.add(gameMove);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //currentTurnTime = gameRules.getTurnTime();
        // We are in the end of the game;
        gameManager.currentGameMove = gameManager.gameMoves.size() - 1;
        deck = gameFactory.getGameDeck(playersHands.size());
        deck.cards.removeAll(deck.cards);

    }

    // Add bot with empty hand
    public void addBot(String level) {
        User bot = gameIA.getBot(this, level);
        playersHands.put(bot, new ArrayList<Card>());
    }

    public int getNumberOfPlayers() {
        return playersHands.size();
    }

    public int getGamePort() {
        return Constants.IN_GAME_CONTROLLER_BASE_RMI_PORT + gameIdentifier;
    }

    public void resetTurnTimer() {
        currentTurnTime = gameRules.getTurnTime();
    }

    public boolean isFinished() {
        return finished;
    }

    public void defineTurnWinner() {
        User user = gameRules.defineTurnWinner();
        int score = 0;

        for (AbstractMap.SimpleEntry<User, Card> pair : table) {
            score += pair.getValue().getCardScore();
        }

        playersScores.put(user, playersScores.get(user) + score);
        turnFirstPlayer = orderedPlayersList.indexOf(user);
        currentPlayer = turnFirstPlayer;
    }

    public void clearTable() {
        table = new ArrayList<AbstractMap.SimpleEntry<User, Card>>();
    }

    public User getWinner() {
        User winner = null;
        int bestScore = -1;
        HashMap<User, Integer> playerList = new HashMap<User, Integer>();
        // Check for best score
        for(Map.Entry<User, Integer> player : playersScores.entrySet())
        {
            if(player.getValue() > bestScore)
            {
                winner = player.getKey();
                bestScore = player.getValue();
            }
            else
            {
                playerList.put(player.getKey(), player.getValue());
            }
        }

        // If two persons have the same score there is no winner
        for(Map.Entry<User, Integer> player : playerList.entrySet())
        {
            if(player.getValue() == bestScore)
                winner = null;
        }

        return winner;
    }

    public int getNewBotIdentifier() {
        botCount++;
        return botCount;
    }

    public String getGameMoveAsString(GameMove gameMove)
    {
        if(gameMove instanceof PlayCard)
            return "PlayCard";
        if(gameMove instanceof GiveCard)
            return "GiveCard";
        return "Unknown";
    }

    public GameMove getStringAsGameMove(String stringGameMove, User user, Card card) {
        if(stringGameMove.equals("PlayCard"))
            return new PlayCard(this, user, card);
        if(stringGameMove.equals("GiveCard"))
            return new GiveCard(card, this, user);

        return null;
    }


/*

    //////////////// Persistent logic after this point

*/

    public int getMoveTypeDatabaseID(GameMove gameMove)
    {
        int moveTypeId = -1;
        String typeName = getGameMoveAsString(gameMove);
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();

        String sqlQuery = "SELECT ID FROM MOVETYPE WHERE TYPE='" + typeName + "';";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);

        try {
            resultSet.next();
            moveTypeId = resultSet.getInt("ID");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sqLiteJDBC.closeConnection();
        return moveTypeId;
    }

    public void saveGame() {
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        HashMap<User, Integer> usersDatabaseID = new HashMap<>();

        String sqlQuery;

        // Get Game Type
        sqlQuery = "SELECT * FROM GAMETYPE WHERE TYPE='" + gameType + "';";
        ResultSet resultSet = sqLiteJDBC.executeQuery(sqlQuery);
        try {
            resultSet.next();
            int gameTypeId = resultSet.getInt("ID");
            resultSet.close();

            // Create Game
            sqlQuery = "INSERT INTO GAME (TYPE, TRUMP, NAME) VALUES(" + gameTypeId + ", '" + trumpSuit + "', '" + gameName + "');";
            Statement statement = sqLiteJDBC.connection.createStatement();
            statement.executeUpdate(sqlQuery);

            // Get game id
            ResultSet gameResultSet = statement.getGeneratedKeys();
            gameResultSet.next();
            int gameID = gameResultSet.getInt(1);
            gameResultSet.close();
            statement.close();


            // Create Each User in the Game
            for(User user: orderedPlayersList)
            {
                int userID;
                if(user instanceof RegisteredUser)
                {
                    userID = ((RegisteredUser) user).getUserDatabaseID();
                }
                else if(user instanceof GameBot)
                {
                    userID = User.CreateUser(user.getUsername(), "GameBot");
                }
                else
                {
                    userID = User.CreateUser(user.getUsername(), "Guest");
                }

                usersDatabaseID.put(user, userID);

                sqlQuery = "INSERT INTO USERGAME (USER, GAME, SCORE, POSITION)" +
                        " VALUES(" + userID +", " + gameID + ", " + playersScores.get(user) + ", " + orderedPlayersList.indexOf(user) + ");";
                sqLiteJDBC.executeUpdate(sqlQuery);
            }

            // Create each GameMove in the database
            for(GameMove gameMove: gameManager.gameMoves)
            {
                int gameMoveID = getMoveTypeDatabaseID(gameMove);
                int gameMoveUserID = usersDatabaseID.get(gameMove.getUser());

                String gameMoveSqlQuery = "INSERT INTO GAMEMOVE (GAME, USER, MOVETYPE, CARDVALUE, CARDSUIT) " +
                        "VALUES (" + gameID + ", " + gameMoveUserID + ", " + gameMoveID + ", '" + gameMove.getCard().value + "', '" + gameMove.getCard().suit + "');";
                sqLiteJDBC.executeUpdate(gameMoveSqlQuery);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        sqLiteJDBC.closeConnection();
    }

}
